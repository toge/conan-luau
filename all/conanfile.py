from conans import ConanFile, CMake, tools
import os
import functools

required_conan_version = ">=1.33.0"

class LuauConan(ConanFile):
    name = "luau"
    description = "A fast, small, safe, gradually typed embeddable scripting language derived from Lua "
    license = "MIT"
    url = "https://github.com/conan-io/conan-center-index"
    homepage = "https://luau-lang.org/"
    topics = ("luau", "lua", "scripting", "typed", "embed")
    settings = "os", "compiler", "arch", "build_type"
    options = {
        "shared": [False, True],
        "fPIC": [True, False],
        "with_cli": [True, False],
        "with_web": [True, False],
    }
    default_options = {
        "shared": False,
        "fPIC": True,
        "with_cli": False,
        "with_web": False,
    }
    generators = "cmake"

    @property
    def _source_subfolder(self):
        return "source_subfolder"

    def export_sources(self):
        self.copy("CMakeLists.txt")
        for patch in self.conan_data.get("patches", {}).get(self.version, []):
            self.copy(patch["patch_file"])

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def configure(self):
        if self.options.shared:
            del self.options.fPIC

    _compiler_required_cpp17 = {
        "Visual Studio": "16",
        "gcc": "8",
        "clang": "7",
        "apple-clang": "12.0",
    }

    def validate(self):
        if self.settings.compiler.get_safe("cppstd"):
            tools.check_min_cppstd(self, 17)

        minimum_version = self._compiler_required_cpp17.get(str(self.settings.compiler), False)
        if minimum_version:
            if tools.Version(self.settings.compiler.version) < minimum_version:
                raise tools.ConanInvalidConfiguration("{} requires C++17, which your compiler does not support.".format(self.name))
        else:
            self.output.warn("{0} requires C++17. Your compiler is unknown. Assuming it supports C++17.".format(self.name))

    def source(self):
        tools.get(**self.conan_data["sources"][self.version],
            destination=self._source_subfolder, strip_root=True)

    @functools.lru_cache(1)
    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions["LUAU_BUILD_CLI"] = self.options.with_cli
        cmake.definitions["LUAU_BUILD_TESTS"] = False
        cmake.definitions["LUAU_BUILD_WEB"] = self.options.with_web
        cmake.definitions["LUAU_WERROR"] = False
        cmake.definitions["LUAU_STATIC_CRT"] = False
        cmake.configure()
        return cmake

    def build(self):
        for patch in self.conan_data.get("patches", {}).get(self.version, []):
            tools.patch(**patch)
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        self.copy(pattern="lua_LICENSE*", dst="licenses", src=self._source_subfolder)
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.set_property("cmake_file_name", "Luau")
        self.cpp_info.set_property("cmake_target_name", "Luau::Luau")

        self.cpp_info.filenames["cmake_find_package"] = "Luau"
        self.cpp_info.filenames["cmake_find_package_multi"] = "Luau"
        self.cpp_info.names["cmake_find_package_multi"] = "Luau"
        self.cpp_info.names["cmake_find_package"] = "Luau"

        self.cpp_info.components["VM"].libs = ["Luau.VM"]
        self.cpp_info.components["VM"].set_property("cmake_target_name", "Luau::VM")
        self.cpp_info.components["VM"].names["cmake_find_package"] = "VM"
        self.cpp_info.components["VM"].names["cmake_find_package_multi"] = "VM"
        if self.settings.os in ["Linux", "FreeBSD"]:
            self.cpp_info.components["VM"].system_libs = ["m"]

        self.cpp_info.components["Ast"].libs = ["Luau.Ast"]
        self.cpp_info.components["Ast"].set_property("cmake_target_name", "Luau::Ast")
        self.cpp_info.components["Ast"].names["cmake_find_package"] = "Ast"
        self.cpp_info.components["Ast"].names["cmake_find_package_multi"] = "Ast"

        self.cpp_info.components["Analysis"].libs = ["Luau.Analysis"]
        self.cpp_info.components["Analysis"].set_property("cmake_target_name", "Luau::Analysis")
        self.cpp_info.components["Analysis"].names["cmake_find_package"] = "Analysis"
        self.cpp_info.components["Analysis"].names["cmake_find_package_multi"] = "Analysis"
        self.cpp_info.components["Analysis"].requires = ["Ast"]

        self.cpp_info.components["Compiler"].libs = ["Luau.Compiler"]
        self.cpp_info.components["Compiler"].set_property("cmake_target_name", "Luau::Compiler")
        self.cpp_info.components["Compiler"].names["cmake_find_package"] = "Compiler"
        self.cpp_info.components["Compiler"].names["cmake_find_package_multi"] = "Compiler"
        self.cpp_info.components["Compiler"].requires = ["Ast"]

        if self.options.with_cli:
            bin_path = os.path.join(self.package_folder, "bin")
            self.output.info("Appending PATH environment variable: {}".format(bin_path))
            self.env_info.PATH.append(bin_path)

        if self.options.with_web:
            self.cpp_info.components["Web"].libs = ["Luau.Web"]
            self.cpp_info.components["Web"].set_property("cmake_target_name", "Luau::Web")
            self.cpp_info.components["Web"].names["cmake_find_package"] = "Web"
            self.cpp_info.components["Web"].names["cmake_find_package_multi"] = "Web"
            self.cpp_info.components["Web"].requires = ["Compiler", "VM"]
